package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Chromium { 



public String name = "Chromium";

public String symbol = "Cr";

public int atomicNumber = 24;

public double atomicWeight = 51.9961;

public double density = 7.18;

public double meltingPoint = 2130;

public double boilingPoint = 2945;

public double atomicRadius = 130;

public double covalentRadius = 118;

public double ionicRadius = 0;

public double specificVolume = 7.23;

public double specificHeat = 0.488;

public double heatFusion = 21;

public double heatEvap = 342;

public double thermalConduct = 93.9;

public double paulingElectro = 1.66;

public double firstIonEnerg = 652.4;

public String oxidationState = "6 3 2 0";

public String electronConfig = "[Ar]3d4s¹";

public String lattice = "BCC";

public double latticeConst = 2.88;
}
