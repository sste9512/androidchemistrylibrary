package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Argon {


    public String name = "Argon";

    public String symbol = "Ar";

    public int atomicNumber = 18;

    public double atomicWeight = 39.948;

    public double density = 1.40;

    public String densityDef = "1.40 (@ -186degC)";

    public double meltingPoint = 83.8;

    public double boilingPoint = 87.3;

    public double atomicRadius = 0;

    public double covalentRadius = 98;

    public double ionicRadius = 0;

    public double specificVolume = 24.2;

    public double specificHeat = 0.138;

    public double heatFusion = 0;

    public double heatEvap = 6.52;

    public double thermalConduct = 0.0177;

    public double paulingElectro = 0;

    public double firstIonEnerg = 1519.6;

    public String oxidationState = "-";

    public String electronConfig = "[Ne]3s²3p";

    public String lattice = "FCC";

    public double latticeConst = 5.26;
}
