package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Mendelevium { 



public String name = "Mendelevium";

public String symbol = "Md";

public int atomicNumber = 101;

public double atomicWeight = 258.1;

public double density = 0;

public double meltingPoint = 1100;

public double boilingPoint = 0;

public double atomicRadius = 287;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 1.3;

public double firstIonEnerg = 635;

public String oxidationState = "3";

public String electronConfig = "[Rn]5f¹³6d7s²";

public String lattice = "-";

public double latticeConst = 0;
}
