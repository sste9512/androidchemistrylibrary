package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Americium {


    public String name = "Americium";

    public String symbol = "Am";

    public int atomicNumber = 95;

    public double atomicWeight = 243.0614;

    public double density = 13.67;

    public double meltingPoint = 1267;

    public double boilingPoint = 2880;

    public double atomicRadius = 173;

    public double covalentRadius = 0;

    public double ionicRadius = 0;

    public double specificVolume = 20.8;

    public double specificHeat = 0;

    public double heatFusion = -10;

    public double heatEvap = 238.5;

    public double thermalConduct = 0;

    public double paulingElectro = 1.3;

    public double firstIonEnerg = 0;

    public String oxidationState = "6 5 4 3";

    public String electronConfig = "[Rn]5f6d7s²";

    public String lattice = "-";

    public double latticeConst = 0;
}
