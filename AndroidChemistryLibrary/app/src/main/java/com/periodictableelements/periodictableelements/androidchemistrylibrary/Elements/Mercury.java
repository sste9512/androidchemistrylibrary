package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Mercury { 



public String name = "Mercury";

public String symbol = "Hg";

public int atomicNumber = 80;

public double atomicWeight = 200.59;

public double density = 13.546;

public String densityDef = "13.546 (@ +20degC)";

public double meltingPoint = 234.28;

public double boilingPoint = 629.73;

public double atomicRadius = 157;

public double covalentRadius = 149;

public double ionicRadius = 0;

public double specificVolume = 14.8;

public double specificHeat = 0.138;

public double heatFusion = 2.295;

public double heatEvap = 58.5;

public double thermalConduct = 8.3;

public double paulingElectro = 2;

public double firstIonEnerg = 1006;

public String oxidationState = "2 1";

public String electronConfig = "[Xe]4f¹5d¹6s²";

public String lattice = "RHL";

public double latticeConst = 2.99;
}
