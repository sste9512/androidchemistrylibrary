package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Iodine { 



public String name = "Iodine";

public String symbol = "I";

public int atomicNumber = 53;

public double atomicWeight = 126.90447;

public double density = 4.93;

public double meltingPoint = 386.7;

public double boilingPoint = 457.5;

public double atomicRadius = 0;

public double covalentRadius = 133;

public double ionicRadius = 0;

public double specificVolume = 25.7;

public double specificHeat = 0.427 ;

public double heatFusion = 15.52 ;

public double heatEvap = 41.95 ;

public double thermalConduct = 0.45;

public double paulingElectro = 2.66;

public double firstIonEnerg = 1008.3;

public String oxidationState = "7 5 1 -1";

public String electronConfig = "[Kr]4d5s²5p";

public String lattice = "ORC";

public double latticeConst = 7.72;
}
