package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Fermium { 



public String name = "Fermium";

public String symbol = "Fm";

public int atomicNumber = 100;

public double atomicWeight = 257.0951;

public double density = 0;

public double meltingPoint = 1800;

public double boilingPoint = 0;

public double atomicRadius = 290;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 1.3;

public double firstIonEnerg = 630;

public String oxidationState = "3";

public String electronConfig = "[Rn]5f¹²6d7s²";

public String lattice = "-";

public double latticeConst = 0;
}
