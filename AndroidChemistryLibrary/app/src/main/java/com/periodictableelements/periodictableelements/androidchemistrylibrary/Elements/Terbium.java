package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Terbium { 



public String name = "Terbium";

public String symbol = "Tb";

public int atomicNumber = 65;

public double atomicWeight = 158.92534;

public double density = 8.229;

public double meltingPoint = 1629;

public double boilingPoint = 3296;

public double atomicRadius = 180;

public double covalentRadius = 159;

public double ionicRadius = 0;

public double specificVolume = 19.2;

public double specificHeat = 0.183;

public double heatFusion = 0;

public double heatEvap = 389;

public double thermalConduct = 11.1;

public double paulingElectro = 1.2;

public double firstIonEnerg = 569;

public String oxidationState = "4 3";

public String electronConfig = "[Xe]4f5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.6;
}
