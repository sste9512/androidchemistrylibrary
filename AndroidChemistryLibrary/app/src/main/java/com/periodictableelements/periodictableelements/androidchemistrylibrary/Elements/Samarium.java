package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Samarium { 



public String name = "Samarium";

public String symbol = "Sm";

public int atomicNumber = 62;

public double atomicWeight = 150.36;

public double density = 7.52;

public double meltingPoint = 1350;

public double boilingPoint = 2064;

public double atomicRadius = 181;

public double covalentRadius = 162;

public double ionicRadius = 0;

public double specificVolume = 19.9;

public double specificHeat = 0.18;

public double heatFusion = 8.9;

public double heatEvap = 165;

public double thermalConduct = 13.3;

public double paulingElectro = 1.17;

public double firstIonEnerg = 540.1;

public String oxidationState = "3 2";

public String electronConfig = "[Xe]4f5d6s²";

public String lattice = "RHL";

public double latticeConst = 9;
}
