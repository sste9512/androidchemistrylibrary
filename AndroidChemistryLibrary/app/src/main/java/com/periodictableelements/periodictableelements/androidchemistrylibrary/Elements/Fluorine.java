package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Fluorine { 



public String name = "Fluorine";

public String symbol = "F";

public int atomicNumber = 9;

public double atomicWeight = 18.998403;

public double density = 1.108;

public String densityDef = "1.108 (@ -189degC)";

public double meltingPoint = 53.53;

public double boilingPoint = 85.01;

public double atomicRadius = 0;

public double covalentRadius = 72;

public double ionicRadius = 0;

public double specificVolume = 17.1;

public double specificHeat = 0.824 ;

public double heatFusion = 0.51;

public double heatEvap = 6.54;

public double thermalConduct = 0.028;

public double paulingElectro = 3.98;

public double firstIonEnerg = 1680;

public String oxidationState = "-1";

public String electronConfig = "[He]2s²2p";

public String lattice = "MCL";

public double latticeConst = 0;
}
