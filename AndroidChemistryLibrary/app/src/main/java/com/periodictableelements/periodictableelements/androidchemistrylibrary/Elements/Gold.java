package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Gold { 



public String name = "Gold";

public String symbol = "Au";

public int atomicNumber = 79;

public double atomicWeight = 196.96654;

public double density = 19.3;

public double meltingPoint = 1337.58;

public double boilingPoint = 3080;

public double atomicRadius = 146;

public double covalentRadius = 134;

public double ionicRadius = 0;

public double specificVolume = 10.2;

public double specificHeat = 0.129;

public double heatFusion = 12.68;

public double heatEvap = ~340;

public double thermalConduct = 318;

public double paulingElectro = 2.54;

public double firstIonEnerg = 889.3;

public String oxidationState = "3 1";

public String electronConfig = "[Xe]4f¹5d¹6s²";

public String lattice = "FCC";

public double latticeConst = 4.08;
}
