package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Nobelium { 



public String name = "Nobelium";

public String symbol = "No";

public int atomicNumber = 102;

public double atomicWeight = 259.1009;

public double density = 0;

public double meltingPoint = 1100;

public double boilingPoint = 0;

public double atomicRadius = 285;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 1.3;

public double firstIonEnerg = 640;

public String oxidationState = "32";

public String electronConfig = "[Rn]5f¹6d7s²";

public String lattice = "-";

public double latticeConst = 0;
}
