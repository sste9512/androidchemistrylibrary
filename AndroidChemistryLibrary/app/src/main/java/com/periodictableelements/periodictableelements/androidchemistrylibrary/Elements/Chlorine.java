package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Chlorine { 



public String name = "Chlorine";

public String symbol = "Cl";

public int atomicNumber = 17;

public double atomicWeight = 35.4527;

public double density = 1.56;

public String densityDef = "1.56 (@ -33.6degC)";

public double meltingPoint = 172.2;

public double boilingPoint = 238.6;

public double atomicRadius = 0;

public double covalentRadius = 99;

public double ionicRadius = 0;

public double specificVolume = 18.7;

public double specificHeat = 0.477 ;

public double heatFusion = 6.41 ;

public double heatEvap = 20.41 ;

public double thermalConduct = 0.009;

public double paulingElectro = 3.16;

public double firstIonEnerg = 1254.9;

public String oxidationState = "7 5 3 1 -1";

public String electronConfig = "[Ne]3s²3p";

public String lattice = "ORC";

public double latticeConst = 6.24;
}
