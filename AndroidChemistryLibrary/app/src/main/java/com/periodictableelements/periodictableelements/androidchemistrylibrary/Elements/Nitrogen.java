package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Nitrogen { 



public String name = "Nitrogen";

public String symbol = "N";

public int atomicNumber = 7;

public double atomicWeight = 14.00674;

public double density = 0.808;

public String densityDef = "0.808 (@ -195.8degC)";

public double meltingPoint = 63.29;

public double boilingPoint = 77.4;

public double atomicRadius = 92;

public double covalentRadius = 75;

public double ionicRadius = 0;

public double specificVolume = 17.3;

public double specificHeat = 1.042;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0.026;

public double paulingElectro = 3.04;

public double firstIonEnerg = 1401.5;

public String oxidationState = "5 4 3 2 -3";

public String electronConfig = "[He]2s²2p³";

public String lattice = "HEX";

public double latticeConst = 4.039;
}
