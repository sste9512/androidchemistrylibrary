package com.periodictableelements.periodictableelements.androidchemistrylibrary;

import java.lang.reflect.Array;

public class ElectronShell {
/*

   THIS CLASS MAY NEED CLEANING UP,

   IT IS A  REFERENCE FOR THE MAXIMUM NUMBER OF ELECTRONS THAT CAN EXIST IN A SUBSHELL

   GO BACK AND MAKE SURE THAT EVERYTHING WAS NAMED PROPERLY (i.e MAY NEED TO BE CALLED VALENCE SHELL)

   CREATE METHOD FOR FINDING OUTER MOST SHELL, INCLUDING CHARGE

 */

    public final ElectronShellTable[] electronShellRef = new ElectronShellTable[]
            {
                    new ElectronShellTable(1, 'K', 2),
                    new ElectronShellTable(2, 'L', 8),
                    new ElectronShellTable(3, 'M', 18),
                    new ElectronShellTable(4, 'N', 32),
                    new ElectronShellTable(5, 'O', 50),
                    new ElectronShellTable(6, 'P', 72)
            };


    public class ElectronShellTable {

        public int energyLevel;
        public char shellLetter;
        public int electronCap;

        public ElectronShellTable(int energyLevel, char shellLetter, int electronCap) {
            this.energyLevel = energyLevel;
            this.shellLetter = shellLetter;
            this.electronCap = electronCap;
        }
    }


}
