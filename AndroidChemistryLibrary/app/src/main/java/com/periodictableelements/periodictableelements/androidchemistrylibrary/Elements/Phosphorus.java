package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Phosphorus { 



public String name = "Phosphorus";

public String symbol = "P";

public int atomicNumber = 15;

public double atomicWeight = 30.973762;

public double density = 1.82;

public double meltingPoint = 317.3;

public double boilingPoint = 553;

public double atomicRadius = 128;

public double covalentRadius = 106;

public double ionicRadius = 0;

public double specificVolume = 17;

public double specificHeat = 0.757;

public double heatFusion = 2.51;

public double heatEvap = 49.8;

public double thermalConduct = 0.236;

public double paulingElectro = 2.19;

public double firstIonEnerg = 1011.2;

public String oxidationState = "5 3 -3";

public String electronConfig = "[Ne]3s²3p³";

public String lattice = "CUB";

public double latticeConst = 7.17;
}
