package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Cerium { 



public String name = "Cerium";

public String symbol = "Ce";

public int atomicNumber = 58;

public double atomicWeight = 140.115;

public double density = 6.757;

public double meltingPoint = 1072;

public double boilingPoint = 3699;

public double atomicRadius = 181;

public double covalentRadius = 165;

public double ionicRadius = 0;

public double specificVolume = 21;

public double specificHeat = 0.205;

public double heatFusion = 5.2;

public double heatEvap = 398;

public double thermalConduct = 11.3;

public double paulingElectro = 1.12;

public double firstIonEnerg = 540.1;

public String oxidationState = "4 3";

public String electronConfig = "[Xe]4f¹5d¹6s²";

public String lattice = "FCC";

public double latticeConst = 5.16;
}
