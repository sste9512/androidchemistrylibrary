package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Xenon { 



public String name = "Xenon";

public String symbol = "Xe";

public int atomicNumber = 54;

public double atomicWeight = 131.29;

public double density = 3.52;

public String densityDef = "3.52 (@ -109degC)";

public double meltingPoint = 161.3;

public double boilingPoint = 166.1;

public double atomicRadius = 0;

public double covalentRadius = 131;

public double ionicRadius = 0;

public double specificVolume = 42.9;

public double specificHeat = 0.158;

public double heatFusion = 0;

public double heatEvap = 12.65;

public double thermalConduct = 0.0057;

public double paulingElectro = 0;

public double firstIonEnerg = 1170;

public String oxidationState = "7";

public String electronConfig = "[Kr]4d5s²5p";

public String lattice = "FCC";

public double latticeConst = 6.2;
}
