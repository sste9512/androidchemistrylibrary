package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Carbon { 



public String name = "Carbon";

public String symbol = "C";

public int atomicNumber = 6;

public double atomicWeight = 12.011;

public double density = 2.25;

public double meltingPoint = 3820;

public double boilingPoint = 5100;

public double atomicRadius = 91;

public double covalentRadius = 77;

public double ionicRadius = 0;

public double specificVolume = 5.3;

public double specificHeat = 0.711;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 1.59;

public double paulingElectro = 2.55;

public double firstIonEnerg = 1085.7;

public String oxidationState = "4 2 -4";

public String electronConfig = "[He]2s²2p²";

public String lattice = "DIA";

public double latticeConst = 3.57;
}
