package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Zirconium { 



public String name = "Zirconium";

public String symbol = "Zr";

public int atomicNumber = 40;

public double atomicWeight = 91.224;

public double density = 6.506;

public double meltingPoint = 2125;

public double boilingPoint = 4650;

public double atomicRadius = 160;

public double covalentRadius = 145;

public double ionicRadius = 0;

public double specificVolume = 14.1;

public double specificHeat = 0.281;

public double heatFusion = 19.2;

public double heatEvap = 567;

public double thermalConduct = 22.7;

public double paulingElectro = 1.33;

public double firstIonEnerg = 659.7;

public String oxidationState = "4";

public String electronConfig = "[Kr]4d²5s²";

public String lattice = "HEX";

public double latticeConst = 3.23;
}
