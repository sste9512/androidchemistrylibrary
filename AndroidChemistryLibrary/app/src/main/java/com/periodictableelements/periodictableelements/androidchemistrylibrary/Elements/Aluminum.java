package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;


public class Aluminum {


    public String name = "Aluminum";

    public String symbol = "Al";

    public int atomicNumber = 13;

    public double atomicWeight = 26.981539;

    public double density = 2.6989;

    public double meltingPoint = 933.5;

    public double boilingPoint = 2740;

    public double atomicRadius = 143;

    public double covalentRadius = 118;

    public double ionicRadius = 0;

    public double specificVolume = 10;

    public double specificHeat = 0.9;

    public double heatFusion = 10.75;

    public double heatEvap = 284.1;

    public double thermalConduct = 237;

    public double paulingElectro = 1.61;

    public double firstIonEnerg = 577.2;

    public String oxidationState = "3";

    public String electronConfig = "[Ne]3s²3p¹";

    public String lattice = "FCC";

    public double latticeConst = 4.05;
}
