package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Iron { 



public String name = "Iron";

public String symbol = "Fe";

public int atomicNumber = 26;

public double atomicWeight = 55.847;

public double density = 7.874;

public double meltingPoint = 1808;

public double boilingPoint = 3023;

public double atomicRadius = 126;

public double covalentRadius = 117;

public double ionicRadius = 0;

public double specificVolume = 7.1;

public double specificHeat = 0.443;

public double heatFusion = 13.8;

public double heatEvap = ~340;

public double thermalConduct = 80.4;

public double paulingElectro = 1.83;

public double firstIonEnerg = 759.1;

public String oxidationState = "6 3 2 0 -2";

public String electronConfig = "[Ar]3d4s²";

public String lattice = "BCC";

public double latticeConst = 2.87;
}
