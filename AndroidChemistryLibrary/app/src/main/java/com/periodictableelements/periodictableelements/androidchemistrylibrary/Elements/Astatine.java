package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Astatine {


    public String name = "Astatine";

    public String symbol = "At";

    public int atomicNumber = 85;

    public double atomicWeight = 209.9871;

    public double density = 0;

    public double meltingPoint = 575;

    public double boilingPoint = 610;

    public double atomicRadius = 0;

    public double covalentRadius = 145;

    public double ionicRadius = 0;

    public double specificVolume = 0;

    public double specificHeat = 0;

    public double heatFusion = 0;

    public double heatEvap = 0;

    public double thermalConduct = 0;

    public double paulingElectro = 2.2;

    public double firstIonEnerg = 916.3;

    public String oxidationState = "7 5 3 1 -1";

    public String electronConfig = "[Xe]4f¹5d¹6s²6p";

    public String lattice = "-";

    public double latticeConst = 0;
}
