package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Lanthanum { 



public String name = "Lanthanum";

public String symbol = "La";

public int atomicNumber = 57;

public double atomicWeight = 138.9055;

public double density = 6.15;

public double meltingPoint = 1194;

public double boilingPoint = 3730;

public double atomicRadius = 187;

public double covalentRadius = 169;

public double ionicRadius = 0;

public double specificVolume = 22.5;

public double specificHeat = 0.197;

public double heatFusion = 8.5;

public double heatEvap = 402;

public double thermalConduct = 13.4;

public double paulingElectro = 1.1;

public double firstIonEnerg = 541.1;

public String oxidationState = "3";

public String electronConfig = "[Xe]6d¹6s²";

public String lattice = "HEX";

public double latticeConst = 3.75;
}
