package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Holmium { 



public String name = "Holmium";

public String symbol = "Ho";

public int atomicNumber = 67;

public double atomicWeight = 164.93032;

public double density = 8.795;

public double meltingPoint = 1747;

public double boilingPoint = 2968;

public double atomicRadius = 179;

public double covalentRadius = 158;

public double ionicRadius = 0;

public double specificVolume = 18.7;

public double specificHeat = 0.164;

public double heatFusion = 0;

public double heatEvap = 301;

public double thermalConduct = 16.2;

public double paulingElectro = 1.23;

public double firstIonEnerg = 574;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f¹¹5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.58;
}
