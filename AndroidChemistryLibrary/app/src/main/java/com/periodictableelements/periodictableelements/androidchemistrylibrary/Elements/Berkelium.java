package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Berkelium {


    public String name = "Berkelium";

    public String symbol = "Bk";

    public int atomicNumber = 97;

    public double atomicWeight = 247.0703;

    public double density = 13.25;

    public double meltingPoint = 0;

    public double boilingPoint = 0;

    public double atomicRadius = 297;

    public double covalentRadius = 0;

    public double ionicRadius = 0;

    public double specificVolume = 0;

    public double specificHeat = 0;

    public double heatFusion = 0;

    public double heatEvap = 0;

    public double thermalConduct = 0;

    public double paulingElectro = 1.3;

    public double firstIonEnerg = 600;

    public String oxidationState = "4 3";

    public String electronConfig = "[Rn]5f6d7s²";

    public String lattice = "-";

    public double latticeConst = 0;
}
