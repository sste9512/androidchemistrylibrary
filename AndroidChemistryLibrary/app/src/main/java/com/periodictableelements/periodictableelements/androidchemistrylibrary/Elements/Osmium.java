package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Osmium { 



public String name = "Osmium";

public String symbol = "Os";

public int atomicNumber = 76;

public double atomicWeight = 190.23;

public double density = 22.57;

public double meltingPoint = 3327;

public double boilingPoint = 5300;

public double atomicRadius = 135;

public double covalentRadius = 126;

public double ionicRadius = 0;

public double specificVolume = 8.43;

public double specificHeat = 0.131;

public double heatFusion = 31.7;

public double heatEvap = 738;

public double thermalConduct = 87.6;

public double paulingElectro = 2.2;

public double firstIonEnerg = 819.8;

public String oxidationState = "8 6 4 3 2 0 -2";

public String electronConfig = "[Xe]4f¹5d6s²";

public String lattice = "HEX";

public double latticeConst = 2.74;
}
