package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Radium { 



public String name = "Radium";

public String symbol = "Ra";

public int atomicNumber = 88;

public double atomicWeight = 226.0254;

public double density = 5.5;

public double meltingPoint = 973;

public double boilingPoint = 1413;

public double atomicRadius = 0;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 45;

public double specificHeat = 0.12;

public double heatFusion = -9.6;

public double heatEvap = -113;

public double thermalConduct = 18.6;

public double paulingElectro = 0.9;

public double firstIonEnerg = 509;

public String oxidationState = "2";

public String electronConfig = "[Rn]7s²";

public String lattice = "-";

public double latticeConst = 0;
}
