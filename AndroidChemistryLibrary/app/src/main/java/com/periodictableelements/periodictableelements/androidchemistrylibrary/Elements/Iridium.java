package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Iridium { 



public String name = "Iridium";

public String symbol = "Ir";

public int atomicNumber = 77;

public double atomicWeight = 192.22;

public double density = 22.42;

public double meltingPoint = 2683;

public double boilingPoint = 4403;

public double atomicRadius = 136;

public double covalentRadius = 127;

public double ionicRadius = 0;

public double specificVolume = 8.54;

public double specificHeat = 0.133;

public double heatFusion = 27.61;

public double heatEvap = 604;

public double thermalConduct = 147;

public double paulingElectro = 2.2;

public double firstIonEnerg = 868.1;

public String oxidationState = "6 4 3 2 1 0 -1";

public String electronConfig = "[Xe]4f¹5d6s²";

public String lattice = "FCC";

public double latticeConst = 3.84;
}
