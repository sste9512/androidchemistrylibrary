package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Europium { 



public String name = "Europium";

public String symbol = "Eu";

public int atomicNumber = 63;

public double atomicWeight = 151.965;

public double density = 5.243;

public double meltingPoint = 1095;

public double boilingPoint = 1870;

public double atomicRadius = 199;

public double covalentRadius = 185;

public double ionicRadius = 0;

public double specificVolume = 28.9;

public double specificHeat = 0.176;

public double heatFusion = 0;

public double heatEvap = 176;

public double thermalConduct = 13.9;

public double paulingElectro = 0;

public double firstIonEnerg = 546.9;

public String oxidationState = "3 2";

public String electronConfig = "[Xe]4f5d6s²";

public String lattice = "BCC";

public double latticeConst = 4.61;
}
