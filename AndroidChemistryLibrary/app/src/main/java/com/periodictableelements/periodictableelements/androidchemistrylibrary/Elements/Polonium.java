package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Polonium { 



public String name = "Polonium";

public String symbol = "Po";

public int atomicNumber = 84;

public double atomicWeight = 208.9824;

public double density = 9.32;

public double meltingPoint = 527;

public double boilingPoint = 1235;

public double atomicRadius = 176;

public double covalentRadius = 146;

public double ionicRadius = 0;

public double specificVolume = 22.7;

public double specificHeat = 0.125;

public double heatFusion = -10;

public double heatEvap = -102.9;

public double thermalConduct = 0;

public double paulingElectro = 2;

public double firstIonEnerg = 813.1;

public String oxidationState = "6 4 2";

public String electronConfig = "[Xe]4f¹5d¹6s²6p";

public String lattice = "SC";

public double latticeConst = 3.35;
}
