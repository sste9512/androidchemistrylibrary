package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Radon { 



public String name = "Radon";

public String symbol = "Rn";

public int atomicNumber = 86;

public double atomicWeight = 222.0176;

public double density = 4.4;

public String densityDef = "4.4 (@ -62degC)";

public double meltingPoint = 202;

public double boilingPoint = 211.4;

public double atomicRadius = 0;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0.094;

public double heatFusion = 0;

public double heatEvap = 18.1;

public double thermalConduct = 0.0036;

public double paulingElectro = 0;

public double firstIonEnerg = 1036.5;

public String oxidationState = "-";

public String electronConfig = "[Xe]4f¹5d¹6s²6p";

public String lattice = "FCC";

public double latticeConst = 0;
}
