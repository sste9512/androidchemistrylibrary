package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Palladium { 



public String name = "Palladium";

public String symbol = "Pd";

public int atomicNumber = 46;

public double atomicWeight = 106.42;

public double density = 12.02;

public double meltingPoint = 1825;

public double boilingPoint = 3413;

public double atomicRadius = 137;

public double covalentRadius = 128;

public double ionicRadius = 0;

public double specificVolume = 8.9;

public double specificHeat = 0.244;

public double heatFusion = 17.24;

public double heatEvap = 372.4;

public double thermalConduct = 71.8;

public double paulingElectro = 2.2;

public double firstIonEnerg = 803.5;

public String oxidationState = "4 2 0";

public String electronConfig = "[Kr]4d5s";

public String lattice = "FCC";

public double latticeConst = 3.89;
}
