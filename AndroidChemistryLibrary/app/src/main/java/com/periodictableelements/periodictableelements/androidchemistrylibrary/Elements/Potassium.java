package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Potassium { 



public String name = "Potassium";

public String symbol = "K";

public int atomicNumber = 19;

public double atomicWeight = 39.0983;

public double density = 0.856;

public double meltingPoint = 336.8;

public double boilingPoint = 1047;

public double atomicRadius = 235;

public double covalentRadius = 203;

public double ionicRadius = 0;

public double specificVolume = 45.3;

public double specificHeat = 0.753;

public double heatFusion = 102.5;

public double heatEvap = 2.33;

public double thermalConduct = 79;

public double paulingElectro = 0.82;

public double firstIonEnerg = 418.5;

public String oxidationState = "1";

public String electronConfig = "[Ar]4s¹";

public String lattice = "BCC";

public double latticeConst = 5.23;
}
