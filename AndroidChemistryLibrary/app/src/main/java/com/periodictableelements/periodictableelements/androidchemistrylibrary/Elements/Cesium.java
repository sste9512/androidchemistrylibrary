package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Cesium { 



public String name = "Cesium";

public String symbol = "Cs";

public int atomicNumber = 55;

public double atomicWeight = 132.90543;

public double density = 1.873;

public double meltingPoint = 301.6;

public double boilingPoint = 951.6;

public double atomicRadius = 267;

public double covalentRadius = 235;

public double ionicRadius = 0;

public double specificVolume = 70;

public double specificHeat = 0.241;

public double heatFusion = 2.09;

public double heatEvap = 68.3;

public double thermalConduct = 35.9;

public double paulingElectro = 0.79;

public double firstIonEnerg = 375.5;

public String oxidationState = "1";

public String electronConfig = "[Xe]6s¹";

public String lattice = "BCC";

public double latticeConst = 6.05;
}
