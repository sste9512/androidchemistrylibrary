package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Germanium { 



public String name = "Germanium";

public String symbol = "Ge";

public int atomicNumber = 32;

public double atomicWeight = 72.61;

public double density = 5.323;

public double meltingPoint = 1210.6;

public double boilingPoint = 3103;

public double atomicRadius = 137;

public double covalentRadius = 122;

public double ionicRadius = 0;

public double specificVolume = 13.6;

public double specificHeat = 0.322;

public double heatFusion = 36.8;

public double heatEvap = 328;

public double thermalConduct = 60.2;

public double paulingElectro = 2.01;

public double firstIonEnerg = 760;

public String oxidationState = "4";

public String electronConfig = "[Ar]3d¹4s²4p²";

public String lattice = "DIA";

public double latticeConst = 5.66;
}
