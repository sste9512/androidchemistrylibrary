package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Bismuth { 



public String name = "Bismuth";

public String symbol = "Bi";

public int atomicNumber = 83;

public double atomicWeight = 208.98037;

public double density = 9.747;

public double meltingPoint = 544.5;

public double boilingPoint = 1883;

public double atomicRadius = 170;

public double covalentRadius = 146;

public double ionicRadius = 0;

public double specificVolume = 21.3;

public double specificHeat = 0.124;

public double heatFusion = 11;

public double heatEvap = 172;

public double thermalConduct = 7.9;

public double paulingElectro = 2.02;

public double firstIonEnerg = 702.9;

public String oxidationState = "5 3";

public String electronConfig = "[Xe]4f¹5d¹6s²6p³";

public String lattice = "RHL";

public double latticeConst = 4.75;
}
