package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Hafnium { 



public String name = "Hafnium";

public String symbol = "Hf";

public int atomicNumber = 72;

public double atomicWeight = 178.49;

public double density = 13.31;

public double meltingPoint = 2503;

public double boilingPoint = 5470;

public double atomicRadius = 167;

public double covalentRadius = 144;

public double ionicRadius = 0;

public double specificVolume = 13.6;

public double specificHeat = 0.146;

public double heatFusion = -25.1;

public double heatEvap = 575;

public double thermalConduct = 23;

public double paulingElectro = 1.3;

public double firstIonEnerg = 575.2;

public String oxidationState = "4";

public String electronConfig = "[Xe]4f¹5d²6s²";

public String lattice = "HEX";

public double latticeConst = 3.2;
}
