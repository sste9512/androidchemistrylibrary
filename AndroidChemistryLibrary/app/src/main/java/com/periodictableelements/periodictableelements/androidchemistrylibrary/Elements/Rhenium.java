package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Rhenium { 



public String name = "Rhenium";

public String symbol = "Re";

public int atomicNumber = 75;

public double atomicWeight = 186.207;

public double density = 21.02;

public double meltingPoint = 3453;

public double boilingPoint = 5900;

public double atomicRadius = 137;

public double covalentRadius = 128;

public double ionicRadius = 0;

public double specificVolume = 8.85;

public double specificHeat = 0.138;

public double heatFusion = 34;

public double heatEvap = 704;

public double thermalConduct = 48;

public double paulingElectro = 1.9;

public double firstIonEnerg = 759.1;

public String oxidationState = "5 4 3 2 -1";

public String electronConfig = "[Xe]4f¹5d6s²";

public String lattice = "HEX";

public double latticeConst = 2.76;
}
