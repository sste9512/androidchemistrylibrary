package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Molybdenum { 



public String name = "Molybdenum";

public String symbol = "Mo";

public int atomicNumber = 42;

public double atomicWeight = 95.94;

public double density = 10.22;

public double meltingPoint = 2890;

public double boilingPoint = 4885;

public double atomicRadius = 139;

public double covalentRadius = 130;

public double ionicRadius = 0;

public double specificVolume = 9.4;

public double specificHeat = 0.251;

public double heatFusion = 28;

public double heatEvap = ~590;

public double thermalConduct = 138;

public double paulingElectro = 2.16;

public double firstIonEnerg = 684.8;

public String oxidationState = "6 5 4 3 2 0";

public String electronConfig = "[Kr]4d5s¹";

public String lattice = "BCC";

public double latticeConst = 3.15;
}
