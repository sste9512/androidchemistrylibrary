package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Uranium { 



public String name = "Uranium";

public String symbol = "U";

public int atomicNumber = 92;

public double atomicWeight = 238.0289;

public double density = 19.05;

public double meltingPoint = 1405.5;

public double boilingPoint = 4018;

public double atomicRadius = 138;

public double covalentRadius = 142;

public double ionicRadius = 0;

public double specificVolume = 12.5;

public double specificHeat = 0.115;

public double heatFusion = 12.6;

public double heatEvap = 417;

public double thermalConduct = 27.5;

public double paulingElectro = 1.38;

public double firstIonEnerg = 686.4;

public String oxidationState = "6 5 4 3";

public String electronConfig = "[Rn]5f³6d¹7s²";

public String lattice = "ORC";

public double latticeConst = 2.85;
}
