package com.periodictableelements.periodictableelements.androidchemistrylibrary;

import com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements.Chlorine;
import com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements.Fluorine;
import com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements.Hydrogen;
import com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements.Nitrogen;
import com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements.Oxygen;

public class ElementCalcUtils {


    public static int getIonCharge(int protons, int electrons) {
        return protons - electrons; }

    public static int getNeutrons(int protons, int neutrons) {
        return protons + neutrons; }

    public static boolean isDiatomicMolecule(Temperature temp, double pressure, Object element)
    {

        if(temp.getCelsius() == 25)
        {
            if (element.equals(Hydrogen.class))
            {
                return true;
            }
            else if (element.equals(Nitrogen.class))
            {
                return true;

            }
            else if (element.equals(Oxygen.class))
            {
                return true;

            }
            else if (element.equals(Fluorine.class))
            {

                return true;
            }
            else if (element.equals(Chlorine.class))
            {
                return true;
            }
            else
                {
                return true;
            }
        }


    }
}
