package com.periodictableelements.periodictableelements.androidchemistrylibrary;

public class PhysicalConstants {

    public static final double AVOGADROS_CONSTANT = (6.0221415) * Math.pow(10f,23f);

    public static final double FARADAY_CONSTANT = (6.24) * Math.pow(10f,18f);

    public static final double ATOMIC_MASS_CONSTANT = (1.66053904) * Math.pow(10f,-27f);

    public static final double MOLAR_GAS_CONSTANT = 1 * Math.pow(10f,-3f); //kg mol -1

    public static final double ASTRONOMICAL_UNIT = 1.4959787 * Math.pow(10f,11f); //METERS

    public static final double BOHR_MAGNETON = 9.274 * Math.pow(10f,-24f); //joule per tesla

    public static final double BOHR_RADIUS = 5.29177 * Math.pow(10f,-11f);//METERS

    public static final double BOLTZMANNS_CONSTANT = 1.3807 * Math.pow(10f,-23f);

    public static final double COMPTON_LENG_ELECTRON = 2.4263 * Math.pow(10f,-12f); //meters

    public static final double COMPTON_LENG_PROTON = 1.3214 * Math.pow(10f,-15f); //meters

    public static final double COMPTON_LENG_NEUTRON = 1.3196 * Math.pow(10f,-15f); //meters

    public static final double ELEMENTARY_CHARGE = 1.60 * Math.pow(10f, -19f);

    public static final double EULERS_CONSTANT = 0.577216;

    public static final double FINE_STRUCTURE_CONSTANT = 0.007297;

    public static final double FIRST_RADIATION_CONSTANT = 4.9926 * Math.pow(10f,-24f);

    public static final double GAS_CONSTANT = 8.3145;

    public static final double GRAVITATIONAL_ACCELERATION_EARTHS_SURFACE = 9.8067;

    public static final double HARTREE_ENERGY = 4.3597 * Math.pow(10f,-18f);

    public static final double GRAVITATIONAL_CONSTANT = 6.67 * Math.pow(10f,-11f);


}
