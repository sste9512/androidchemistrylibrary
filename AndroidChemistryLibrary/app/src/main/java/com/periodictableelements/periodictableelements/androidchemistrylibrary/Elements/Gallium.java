package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Gallium { 



public String name = "Gallium";

public String symbol = "Ga";

public int atomicNumber = 31;

public double atomicWeight = 69.723;

public double density = 5.91;

public double meltingPoint = 302.93;

public double boilingPoint = 2676;

public double atomicRadius = 141;

public double covalentRadius = 126;

public double ionicRadius = 0;

public double specificVolume = 11.8;

public double specificHeat = 0.372;

public double heatFusion = 5.59;

public double heatEvap = 270.3;

public double thermalConduct = 28.1;

public double paulingElectro = 1.81;

public double firstIonEnerg = 578.7;

public String oxidationState = "3";

public String electronConfig = "[Ar]3d¹4s²4p¹";

public String lattice = "ORC";

public double latticeConst = 4.51;
}
