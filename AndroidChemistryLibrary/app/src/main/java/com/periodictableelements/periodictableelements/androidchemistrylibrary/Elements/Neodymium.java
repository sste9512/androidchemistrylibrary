package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Neodymium { 



public String name = "Neodymium";

public String symbol = "Nd";

public int atomicNumber = 60;

public double atomicWeight = 144.24;

public double density = 7.007;

public double meltingPoint = 1294;

public double boilingPoint = 3341;

public double atomicRadius = 182;

public double covalentRadius = 184;

public double ionicRadius = 0;

public double specificVolume = 20.6;

public double specificHeat = 0.205;

public double heatFusion = 7.1;

public double heatEvap = 289;

public double thermalConduct = 16.5;

public double paulingElectro = 1.14;

public double firstIonEnerg = 531.5;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.66;
}
