package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Hassium { 



public String name = "Hassium";

public String symbol = "Hs";

public int atomicNumber = 108;

public double atomicWeight = 269;

public double density = 0;

public double meltingPoint = 0;

public double boilingPoint = 0;

public double atomicRadius = 0;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 0;

public double firstIonEnerg = 0;

public String oxidationState = "-";

public String electronConfig = "-";

public String lattice = "-";

public double latticeConst = 0;
}
