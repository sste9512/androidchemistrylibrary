package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Arsenic {


    public String name = "Arsenic";

    public String symbol = "As";

    public int atomicNumber = 33;

    public double atomicWeight = 74.92159;

    public double density = 5.73;

    public double meltingPoint = 1090;

    public double boilingPoint = 876;

    public double atomicRadius = 139;

    public double covalentRadius = 120;

    public double ionicRadius = 0;

    public double specificVolume = 13.1;

    public double specificHeat = 0.328;

    public double heatFusion = 0;

    public double heatEvap = 32.4;

    public double thermalConduct = 50.2;

    public double paulingElectro = 2.18;

    public double firstIonEnerg = 946.2;

    public String oxidationState = "5 3 -2";

    public String electronConfig = "[Ar]3d¹4s²4p³";

    public String lattice = "RHL";

    public double latticeConst = 4.13;
}
