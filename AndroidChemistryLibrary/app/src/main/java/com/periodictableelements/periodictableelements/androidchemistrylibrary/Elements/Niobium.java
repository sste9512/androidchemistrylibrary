package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Niobium { 



public String name = "Niobium";

public String symbol = "Nb";

public int atomicNumber = 41;

public double atomicWeight = 92.90638;

public double density = 8.57;

public double meltingPoint = 2741;

public double boilingPoint = 5015;

public double atomicRadius = 146;

public double covalentRadius = 134;

public double ionicRadius = 0;

public double specificVolume = 10.8;

public double specificHeat = 0.268;

public double heatFusion = 26.8;

public double heatEvap = 680;

public double thermalConduct = 53.7;

public double paulingElectro = 1.6;

public double firstIonEnerg = 663.6;

public String oxidationState = "5 3";

public String electronConfig = "[Kr]4d5s¹";

public String lattice = "BCC";

public double latticeConst = 3.3;
}
