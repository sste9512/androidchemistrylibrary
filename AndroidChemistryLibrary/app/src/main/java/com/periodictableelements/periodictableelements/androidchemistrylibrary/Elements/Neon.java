package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Neon { 



public String name = "Neon";

public String symbol = "Ne";

public int atomicNumber = 10;

public double atomicWeight = 20.1797;

public double density = 1.204;

public String densityDef = "1.204 (@ -246degC)";

public double meltingPoint = 48;

public double boilingPoint = 27.1;

public double atomicRadius = 0;

public double covalentRadius = 71;

public double ionicRadius = 0;

public double specificVolume = 16.8;

public double specificHeat = 1.029;

public double heatFusion = 0;

public double heatEvap = 1.74;

public double thermalConduct = 0.0493;

public double paulingElectro = 0;

public double firstIonEnerg = 2079.4;

public String oxidationState = "-";

public String electronConfig = "[He]2s²2p";

public String lattice = "FCC";

public double latticeConst = 4.43;
}
