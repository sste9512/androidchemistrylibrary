package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Antimony {


    public String name = "Antimony";

    public String symbol = "Sb";

    public int atomicNumber = 51;

    public double atomicWeight = 121.76;

    public double density = 6.691;

    public double meltingPoint = 903.9;

    public double boilingPoint = 1908;

    public double atomicRadius = 159;

    public double covalentRadius = 140;

    public double ionicRadius = 0;

    public double specificVolume = 18.4;

    public double specificHeat = 0.205;

    public double heatFusion = 20.08;

    public double heatEvap = 195.2;

    public double thermalConduct = 24.43;

    public double paulingElectro = 2.05;

    public double firstIonEnerg = 833.3;

    public String oxidationState = "5 3 -2";

    public String electronConfig = "[Kr]4d5s²5p³";

    public String lattice = "RHL";

    public double latticeConst = 4.51;
}
