package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Zinc { 



public String name = "Zinc";

public String symbol = "Zn";

public int atomicNumber = 30;

public double atomicWeight = 65.39;

public double density = 7.133;

public double meltingPoint = 692.73;

public double boilingPoint = 1180;

public double atomicRadius = 138;

public double covalentRadius = 125;

public double ionicRadius = 0;

public double specificVolume = 9.2;

public double specificHeat = 0.388;

public double heatFusion = 7.28;

public double heatEvap = 114.8;

public double thermalConduct = 116;

public double paulingElectro = 1.65;

public double firstIonEnerg = 905.8;

public String oxidationState = "2";

public String electronConfig = "[Ar]3d¹4s²";

public String lattice = "HEX";

public double latticeConst = 2.66;
}
