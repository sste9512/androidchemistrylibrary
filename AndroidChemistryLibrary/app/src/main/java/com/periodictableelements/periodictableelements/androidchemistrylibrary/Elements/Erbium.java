package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Erbium { 



public String name = "Erbium";

public String symbol = "Er";

public int atomicNumber = 68;

public double atomicWeight = 167.26;

public double density = 9.06;

public double meltingPoint = 1802;

public double boilingPoint = 3136;

public double atomicRadius = 178;

public double covalentRadius = 157;

public double ionicRadius = 0;

public double specificVolume = 18.4;

public double specificHeat = 0.168;

public double heatFusion = 0;

public double heatEvap = 317;

public double thermalConduct = 14.5;

public double paulingElectro = 1.24;

public double firstIonEnerg = 581;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f¹²5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.56;
}
