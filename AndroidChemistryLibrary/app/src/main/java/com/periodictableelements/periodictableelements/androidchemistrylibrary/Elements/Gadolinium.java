package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Gadolinium { 



public String name = "Gadolinium";

public String symbol = "Gd";

public int atomicNumber = 64;

public double atomicWeight = 157.25;

public double density = 7.9;

public double meltingPoint = 1586;

public double boilingPoint = 3539;

public double atomicRadius = 179;

public double covalentRadius = 161;

public double ionicRadius = 0;

public double specificVolume = 19.9;

public double specificHeat = 0.23;

public double heatFusion = 0;

public double heatEvap = 398;

public double thermalConduct = 10.5;

public double paulingElectro = 1.2;

public double firstIonEnerg = 594.2;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f5d¹6s²";

public String lattice = "HEX";

public double latticeConst = 3.64;
}
