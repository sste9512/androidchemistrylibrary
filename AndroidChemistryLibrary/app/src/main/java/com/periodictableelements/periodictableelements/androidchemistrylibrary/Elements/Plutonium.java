package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Plutonium { 



public String name = "Plutonium";

public String symbol = "Pu";

public int atomicNumber = 94;

public double atomicWeight = 244.0642;

public double density = 19.84;

public double meltingPoint = 914;

public double boilingPoint = 3505;

public double atomicRadius = 151;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 2.8;

public double heatEvap = 343.5;

public double thermalConduct = 6.7;

public double paulingElectro = 1.28;

public double firstIonEnerg = 491.9;

public String oxidationState = "6 5 4 3";

public String electronConfig = "[Rn]5f6d7s²";

public String lattice = "MCL";

public double latticeConst = 0;
}
