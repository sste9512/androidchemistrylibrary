package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Silicon { 



public String name = "Silicon";

public String symbol = "Si";

public int atomicNumber = 14;

public double atomicWeight = 28.0855;

public double density = 2.33;

public double meltingPoint = 1683;

public double boilingPoint = 2628;

public double atomicRadius = 132;

public double covalentRadius = 111;

public double ionicRadius = 0;

public double specificVolume = 12.1;

public double specificHeat = 0.703;

public double heatFusion = 50.6;

public double heatEvap = 383;

public double thermalConduct = 149;

public double paulingElectro = 1.9;

public double firstIonEnerg = 786;

public String oxidationState = "4 -4";

public String electronConfig = "[Ne]3s²3p²";

public String lattice = "DIA";

public double latticeConst = 5.43;
}
