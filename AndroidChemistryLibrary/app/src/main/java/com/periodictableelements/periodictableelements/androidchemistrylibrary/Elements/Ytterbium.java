package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Ytterbium { 



public String name = "Ytterbium";

public String symbol = "Yb";

public int atomicNumber = 70;

public double atomicWeight = 173.04;

public double density = 6.9654;

public double meltingPoint = 1097;

public double boilingPoint = 1466;

public double atomicRadius = 194;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 24.8;

public double specificHeat = 0.145;

public double heatFusion = 3.35;

public double heatEvap = 159;

public double thermalConduct = 34.9;

public double paulingElectro = 1.1;

public double firstIonEnerg = 603;

public String oxidationState = "3 2";

public String electronConfig = "[Xe]4f¹5d¹6s²";

public String lattice = "FCC";

public double latticeConst = 5.49;
}
