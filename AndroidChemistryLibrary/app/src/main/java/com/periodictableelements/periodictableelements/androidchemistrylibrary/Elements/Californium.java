package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Californium { 



public String name = "Californium";

public String symbol = "Cf";

public int atomicNumber = 98;

public double atomicWeight = 251.0796;

public double density = 15.1;

public double meltingPoint = 900;

public double boilingPoint = 0;

public double atomicRadius = 295;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 1.3;

public double firstIonEnerg = 610;

public String oxidationState = "4 3";

public String electronConfig = "[Rn]5f¹6d7s²";

public String lattice = "-";

public double latticeConst = 0;
}
