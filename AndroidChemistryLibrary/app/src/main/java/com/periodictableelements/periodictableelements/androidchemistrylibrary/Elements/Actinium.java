package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;

public class Actinium {


    public String name = "Actinium";

    public String symbol = "Ac";

    public int atomicNumber = 89;

    public double atomicWeight = 227.0278;

    public double density = 0;

    public double meltingPoint = 1320;

    public double boilingPoint = 3470;

    public double atomicRadius = 188;

    public double covalentRadius = 0;

    public double ionicRadius = 0;

    public double specificVolume = 22.54;

    public double specificHeat = 0;

    public double heatFusion = -10.5;

    public double heatEvap = -292.9;

    public double thermalConduct = 0;

    public double paulingElectro = 1.1;

    public double firstIonEnerg = 665.5;

    public String oxidationState = "3";

    public String electronConfig = "[Rn]6d¹7s²";

    public String lattice = "FCC";

    public double latticeConst = 5.31;
}
