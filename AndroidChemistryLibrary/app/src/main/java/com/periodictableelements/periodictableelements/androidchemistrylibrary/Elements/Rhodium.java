package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Rhodium { 



public String name = "Rhodium";

public String symbol = "Rh";

public int atomicNumber = 45;

public double atomicWeight = 102.9055;

public double density = 12.41;

public double meltingPoint = 2239;

public double boilingPoint = 4000;

public double atomicRadius = 134;

public double covalentRadius = 125;

public double ionicRadius = 0;

public double specificVolume = 8.3;

public double specificHeat = 0.244;

public double heatFusion = 21.8;

public double heatEvap = 494;

public double thermalConduct = 150;

public double paulingElectro = 2.28;

public double firstIonEnerg = 719.5;

public String oxidationState = "5 4 3 2 1 0";

public String electronConfig = "[Kr]4d5s¹";

public String lattice = "FCC";

public double latticeConst = 3.8;
}
