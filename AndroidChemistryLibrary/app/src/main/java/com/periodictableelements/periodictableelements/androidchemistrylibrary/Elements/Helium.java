package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Helium { 



public String name = "Helium";

public String symbol = "He";

public int atomicNumber = 2;

public double atomicWeight = 4.002602;

public double density = 0.147;

public String densityDef = "0.147 (@ -270degC)";

public double meltingPoint = 0.95;

public double boilingPoint = 4.216;

public double atomicRadius = 0;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 31.8;

public double specificHeat = 5.188;

public double heatFusion = 0;

public double heatEvap = 0.08;

public double thermalConduct = 0.152;

public double paulingElectro = 0;

public double firstIonEnerg = 2361.3;

public String oxidationState = "-";

public String electronConfig = "1s²";

public String lattice = "HEX";

public double latticeConst = 3.57;
}
