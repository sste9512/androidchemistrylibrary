package com.periodictableelements.periodictableelements.androidchemistrylibrary;


import com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements.*;

public class PeriodicElements
{

    public static Object findElementByNumber(int chemNumber) {
        switch(chemNumber)
        {

            case 1 :
                return new Hydrogen();

            case 2 :
                return new Helium();

            case 3 :
                return new Lithium();

            case 4 :
                return new Beryllium();

            case 5 :
                return new Boron();

            case 6 :
                return new Carbon();

            case 7 :
                return new Nitrogen();

            case 8 :
                return new Oxygen();

            case 9 :
                return new Fluorine();

            case 10 :
                return new Neon();

            case 11 :
                return new Sodium();

            case 12 :
                return new Magnesium();

            case 13 :
                return new Aluminum();

            case 14 :
                return new Silicon();

            case 15 :
                return new Phosphorus();

            case 16 :
                return new Sulfur();

            case 17 :
                return new Chlorine();

            case 18 :
                return new Argon();

            case 19 :
                return new Potassium();

            case 20 :
                return new Calcium();

            case 21 :
                return new Scandium();

            case 22 :
                return new Titanium();

            case 23 :
                return new Vanadium();

            case 24 :
                return new Chromium();

            case 25 :
                return new Manganese();

            case 26 :
                return new Iron();

            case 27 :
                return new Cobalt();

            case 28 :
                return new Nickel();

            case 29 :
                return new Copper();

            case 30 :
                return new Zinc();

            case 31 :
                return new Gallium();

            case 32 :
                return new Germanium();

            case 33 :
                return new Arsenic();

            case 34 :
                return new Selenium();

            case 35 :
                return new Bromine();

            case 36 :
                return new Krypton();

            case 37 :
                return new Rubidium();

            case 38 :
                return new Strontium();

            case 39 :
                return new Yttrium();

            case 40 :
                return new Zirconium();

            case 41 :
                return new Niobium();

            case 42 :
                return new Molybdenum();

            case 43 :
                return new Technetium();

            case 44 :
                return new Ruthenium();

            case 45 :
                return new Rhodium();

            case 46 :
                return new Palladium();

            case 47 :
                return new Silver();

            case 48 :
                return new Cadmium();

            case 49 :
                return new Indium();

            case 50 :
                return new Tin();

            case 51 :
                return new Antimony();

            case 52 :
                return new Tellurium();

            case 53 :
                return new Iodine();

            case 54 :
                return new Xenon();

            case 55 :
                return new Cesium();

            case 56 :
                return new Barium();

            case 57 :
                return new Lanthanum();

            case 58 :
                return new Cerium();

            case 59 :
                return new Praseodymium();

            case 60 :
                return new Neodymium();

            case 61 :
                return new Promethium();

            case 62 :
                return new Samarium();

            case 63 :
                return new Europium();

            case 64 :
                return new Gadolinium();

            case 65 :
                return new Terbium();

            case 66 :
                return new Dysprosium();

            case 67 :
                return new Holmium();

            case 68 :
                return new Erbium();

            case 69 :
                return new Thulium();

            case 70 :
                return new Ytterbium();

            case 71 :
                return new Lutetium();

            case 72 :
                return new Hafnium();

            case 73 :
                return new Tantalum();

            case 74 :
                return new Tungsten();

            case 75 :
                return new Rhenium();

            case 76 :
                return new Osmium();

            case 77 :
                return new Iridium();

            case 78 :
                return new Platinum();

            case 79 :
                return new Gold();

            case 80 :
                return new Mercury();

            case 81 :
                return new Thallium();

            case 82 :
                return new Lead();

            case 83 :
                return new Bismuth();

            case 84 :
                return new Polonium();

            case 85 :
                return new Astatine();

            case 86 :
                return new Radon();

            case 87 :
                return new Francium();

            case 88 :
                return new Radium();

            case 89 :
                return new Actinium();

            case 90 :
                return new Thorium();

            case 91 :
                return new Protactinium();

            case 92 :
                return new Uranium();

            case 93 :
                return new Neptunium();

            case 94 :
                return new Plutonium();

            case 95 :
                return new Americium();

            case 96 :
                return new Curium();

            case 97 :
                return new Berkelium();

            case 98 :
                return new Californium();

            case 99 :
                return new Einsteinium();

            case 100 :
                return new Fermium();

            case 101 :
                return new Mendelevium();

            case 102 :
                return new Nobelium();

            case 103 :
                return new Lawrencium();

            case 104 :
                return new Rutherfordium();

            case 105 :
                return new Dubnium();

            case 106 :
                return new Seaborgium();

            case 107 :
                return new Bohrium();

            case 108 :
                return new Hassium();

            case 109 :
                return new Meitnerium();

            case 110 :
                return new Ununnilium();

            case 111 :
                return new Unununium();

            case 112 :
                return new Ununbium();

            case 113 :
                return new Ununtrium();

            case 114 :
                return new Ununquadium();

            case 115 :
                return new Ununpentium();

            case 116 :
                return new Ununhexium();

            case 117 :
                return new Ununseptium();

            case 118 :
                return new Ununoctium();

            default :

                return new Hydrogen();
        }
    }

    public static Object findElementByName(String chemName) {
    switch (chemName) {

        case "Hydrogen":
            return new Hydrogen();

        case "Helium":
            return new Helium();

        case "Lithium":
            return new Lithium();

        case "Beryllium":
            return new Beryllium();

        case "Boron":
            return new Boron();

        case "Carbon":
            return new Carbon();

        case "Nitrogen":
            return new Nitrogen();

        case "Oxygen":
            return new Oxygen();

        case "Fluorine":
            return new Fluorine();

        case "Neon":
            return new Neon();

        case "Sodium":
            return new Sodium();

        case "Magnesium":
            return new Magnesium();

        case "Aluminum":
            return new Aluminum();

        case "Silicon":
            return new Silicon();

        case "Phosphorus":
            return new Phosphorus();

        case "Sulfur":
            return new Sulfur();

        case "Chlorine":
            return new Chlorine();

        case "Argon":
            return new Argon();

        case "Potassium":
            return new Potassium();

        case "Calcium":
            return new Calcium();

        case "Scandium":
            return new Scandium();

        case "Titanium":
            return new Titanium();

        case "Vanadium":
            return new Vanadium();

        case "Chromium":
            return new Chromium();

        case "Manganese":
            return new Manganese();

        case "Iron":
            return new Iron();

        case "Cobalt":
            return new Cobalt();

        case "Nickel":
            return new Nickel();

        case "Copper":
            return new Copper();

        case "Zinc":
            return new Zinc();

        case "Gallium":
            return new Gallium();

        case "Germanium":
            return new Germanium();

        case "Arsenic":
            return new Arsenic();

        case "Selenium":
            return new Selenium();

        case "Bromine":
            return new Bromine();

        case "Krypton":
            return new Krypton();

        case "Rubidium":
            return new Rubidium();

        case "Strontium":
            return new Strontium();

        case "Yttrium":
            return new Yttrium();

        case "Zirconium":
            return new Zirconium();

        case "Niobium":
            return new Niobium();

        case "Molybdenum":
            return new Molybdenum();

        case "Technetium":
            return new Technetium();

        case "Ruthenium":
            return new Ruthenium();

        case "Rhodium":
            return new Rhodium();

        case "Palladium":
            return new Palladium();

        case "Silver":
            return new Silver();

        case "Cadmium":
            return new Cadmium();

        case "Indium":
            return new Indium();

        case "Tin":
            return new Tin();

        case "Antimony":
            return new Antimony();

        case "Tellurium":
            return new Tellurium();

        case "Iodine":
            return new Iodine();

        case "Xenon":
            return new Xenon();

        case "Cesium":
            return new Cesium();

        case "Barium":
            return new Barium();

        case "Lanthanum":
            return new Lanthanum();

        case "Cerium":
            return new Cerium();

        case "Praseodymium":
            return new Praseodymium();

        case "Neodymium":
            return new Neodymium();

        case "Promethium":
            return new Promethium();

        case "Samarium":
            return new Samarium();

        case "Europium":
            return new Europium();

        case "Gadolinium":
            return new Gadolinium();

        case "Terbium":
            return new Terbium();

        case "Dysprosium":
            return new Dysprosium();

        case "Holmium":
            return new Holmium();

        case "Erbium":
            return new Erbium();

        case "Thulium":
            return new Thulium();

        case "Ytterbium":
            return new Ytterbium();

        case "Lutetium":
            return new Lutetium();

        case "Hafnium":
            return new Hafnium();

        case "Tantalum":
            return new Tantalum();

        case "Tungsten":
            return new Tungsten();

        case "Rhenium":
            return new Rhenium();

        case "Osmium":
            return new Osmium();

        case "Iridium":
            return new Iridium();

        case "Platinum":
            return new Platinum();

        case "Gold":
            return new Gold();

        case "Mercury":
            return new Mercury();

        case "Thallium":
            return new Thallium();

        case "Lead":
            return new Lead();

        case "Bismuth":
            return new Bismuth();

        case "Polonium":
            return new Polonium();

        case "Astatine":
            return new Astatine();

        case "Radon":
            return new Radon();

        case "Francium":
            return new Francium();

        case "Radium":
            return new Radium();

        case "Actinium":
            return new Actinium();

        case "Thorium":
            return new Thorium();

        case "Protactinium":
            return new Protactinium();

        case "Uranium":
            return new Uranium();

        case "Neptunium":
            return new Neptunium();

        case "Plutonium":
            return new Plutonium();

        case "Americium":
            return new Americium();

        case "Curium":
            return new Curium();

        case "Berkelium":
            return new Berkelium();

        case "Californium":
            return new Californium();

        case "Einsteinium":
            return new Einsteinium();

        case "Fermium":
            return new Fermium();

        case "Mendelevium":
            return new Mendelevium();

        case "Nobelium":
            return new Nobelium();

        case "Lawrencium":
            return new Lawrencium();

        case "Rutherfordium":
            return new Rutherfordium();

        case "Dubnium":
            return new Dubnium();

        case "Seaborgium":
            return new Seaborgium();

        case "Bohrium":
            return new Bohrium();

        case "Hassium":
            return new Hassium();

        case "Meitnerium":
            return new Meitnerium();

        case "Ununnilium":
            return new Ununnilium();

        case "Unununium":
            return new Unununium();

        case "Ununbium":
            return new Ununbium();

        case "Ununtrium":
            return new Ununtrium();

        case "Ununquadium":
            return new Ununquadium();

        case "Ununpentium":
            return new Ununpentium();

        case "Ununhexium":
            return new Ununhexium();

        case "Ununseptium":
            return new Ununseptium();

        case "Ununoctium":
            return new Ununoctium();

        default:

            return new Hydrogen();

    }
}

    public static Object findElementBySymbol(String chemSymbol) {
        switch (chemSymbol) {

            case "H":
                return new Hydrogen();

            case "He":
                return new Helium();

            case "Li":
                return new Lithium();

            case "Be":
                return new Beryllium();

            case "B":
                return new Boron();

            case "C":
                return new Carbon();

            case "N":
                return new Nitrogen();

            case "O":
                return new Oxygen();

            case "F":
                return new Fluorine();

            case "Ne":
                return new Neon();

            case "Na":
                return new Sodium();

            case "Mg":
                return new Magnesium();

            case "Al":
                return new Aluminum();

            case "Si":
                return new Silicon();

            case "P":
                return new Phosphorus();

            case "S":
                return new Sulfur();

            case "Cl":
                return new Chlorine();

            case "Ar":
                return new Argon();

            case "K":
                return new Potassium();

            case "Ca":
                return new Calcium();

            case "Sc":
                return new Scandium();

            case "Ti":
                return new Titanium();

            case "V":
                return new Vanadium();

            case "Cr":
                return new Chromium();

            case "Mn":
                return new Manganese();

            case "Fe":
                return new Iron();

            case "Co":
                return new Cobalt();

            case "Ni":
                return new Nickel();

            case "Cu":
                return new Copper();

            case "Zn":
                return new Zinc();

            case "Ga":
                return new Gallium();

            case "Ge":
                return new Germanium();

            case "As":
                return new Arsenic();

            case "Se":
                return new Selenium();

            case "Br":
                return new Bromine();

            case "Kr":
                return new Krypton();

            case "Rb":
                return new Rubidium();

            case "Sr":
                return new Strontium();

            case "Y":
                return new Yttrium();

            case "Zr":
                return new Zirconium();

            case "Nb":
                return new Niobium();

            case "Mo":
                return new Molybdenum();

            case "Tc":
                return new Technetium();

            case "Ru":
                return new Ruthenium();

            case "Rh":
                return new Rhodium();

            case "Pd":
                return new Palladium();

            case "Ag":
                return new Silver();

            case "Cd":
                return new Cadmium();

            case "In":
                return new Indium();

            case "Sn":
                return new Tin();

            case "Sb":
                return new Antimony();

            case "Te":
                return new Tellurium();

            case "I":
                return new Iodine();

            case "Xe":
                return new Xenon();

            case "Cs":
                return new Cesium();

            case "Ba":
                return new Barium();

            case "La":
                return new Lanthanum();

            case "Ce":
                return new Cerium();

            case "Pr":
                return new Praseodymium();

            case "Nd":
                return new Neodymium();

            case "Pm":
                return new Promethium();

            case "Sm":
                return new Samarium();

            case "Eu":
                return new Europium();

            case "Gd":
                return new Gadolinium();

            case "Tb":
                return new Terbium();

            case "Dy":
                return new Dysprosium();

            case "Ho":
                return new Holmium();

            case "Er":
                return new Erbium();

            case "Tm":
                return new Thulium();

            case "Yb":
                return new Ytterbium();

            case "Lu":
                return new Lutetium();

            case "Hf":
                return new Hafnium();

            case "Ta":
                return new Tantalum();

            case "W":
                return new Tungsten();

            case "Re":
                return new Rhenium();

            case "Os":
                return new Osmium();

            case "Ir":
                return new Iridium();

            case "Pt":
                return new Platinum();

            case "Au":
                return new Gold();

            case "Hg":
                return new Mercury();

            case "Tl":
                return new Thallium();

            case "Pb":
                return new Lead();

            case "Bi":
                return new Bismuth();

            case "Po":
                return new Polonium();

            case "At":
                return new Astatine();

            case "Rn":
                return new Radon();

            case "Fr":
                return new Francium();

            case "Ra":
                return new Radium();

            case "Ac":
                return new Actinium();

            case "Th":
                return new Thorium();

            case "Pa":
                return new Protactinium();

            case "U":
                return new Uranium();

            case "Np":
                return new Neptunium();

            case "Pu":
                return new Plutonium();

            case "Am":
                return new Americium();

            case "Cm":
                return new Curium();

            case "Bk":
                return new Berkelium();

            case "Cf":
                return new Californium();

            case "Es":
                return new Einsteinium();

            case "Fm":
                return new Fermium();

            case "Md":
                return new Mendelevium();

            case "No":
                return new Nobelium();

            case "Lr":
                return new Lawrencium();

            case "Rf":
                return new Rutherfordium();

            case "Db":
                return new Dubnium();

            case "Sg":
                return new Seaborgium();

            case "Bh":
                return new Bohrium();

            case "Hs":
                return new Hassium();

            case "Mt":
                return new Meitnerium();

            case "Uun":
                return new Ununnilium();

            case "Uuu":
                return new Unununium();

            case "Uub":
                return new Ununbium();

            case "Uut":
                return new Ununtrium();

            case "Uuq":
                return new Ununquadium();

            case "Uup":
                return new Ununpentium();

            case "Uuh":
                return new Ununhexium();

            case "Uus":
                return new Ununseptium();

            case "Uuo":
                return new Ununoctium();

            default:

                return new Hydrogen();

        }
    }


}
