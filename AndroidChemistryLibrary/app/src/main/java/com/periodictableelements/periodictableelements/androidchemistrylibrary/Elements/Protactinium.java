package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Protactinium { 



public String name = "Protactinium";

public String symbol = "Pa";

public int atomicNumber = 91;

public double atomicWeight = 231.03588;

public double density = 15.37;

public double meltingPoint = 2113;

public double boilingPoint = 4300;

public double atomicRadius = 161;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 15;

public double specificHeat = 0.121;

public double heatFusion = 16.7;

public double heatEvap = 481.2;

public double thermalConduct = 0;

public double paulingElectro = 1.5;

public double firstIonEnerg = 0;

public String oxidationState = "5 4";

public String electronConfig = "[Rn]5f²6d¹7s²";

public String lattice = "TET";

public double latticeConst = 3.92;
}
