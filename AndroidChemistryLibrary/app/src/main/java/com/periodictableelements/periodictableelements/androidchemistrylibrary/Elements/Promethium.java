package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Promethium { 



public String name = "Promethium";

public String symbol = "Pm";

public int atomicNumber = 61;

public double atomicWeight = 144.9127;

public double density = 7.2;

public double meltingPoint = 1441;

public double boilingPoint = 3000;

public double atomicRadius = 0;

public double covalentRadius = 163;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0.185;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 17.9;

public double paulingElectro = 0;

public double firstIonEnerg = 536;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f5d6s²";

public String lattice = "-";

public double latticeConst = 0;
}
