package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Curium { 



public String name = "Curium";

public String symbol = "Cm";

public int atomicNumber = 96;

public double atomicWeight = 247.0703;

public double density = 13.51;

public double meltingPoint = 1340;

public double boilingPoint = 0;

public double atomicRadius = 299;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 18.28;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 1.3;

public double firstIonEnerg = 580;

public String oxidationState = "4 3";

public String electronConfig = "[Rn]5f6d¹7s²";

public String lattice = "-";

public double latticeConst = 0;
}
