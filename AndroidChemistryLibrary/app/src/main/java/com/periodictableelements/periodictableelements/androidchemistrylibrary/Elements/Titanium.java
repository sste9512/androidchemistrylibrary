package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Titanium { 



public String name = "Titanium";

public String symbol = "Ti";

public int atomicNumber = 22;

public double atomicWeight = 47.88;

public double density = 4.54;

public double meltingPoint = 1933;

public double boilingPoint = 3560;

public double atomicRadius = 147;

public double covalentRadius = 132;

public double ionicRadius = 0;

public double specificVolume = 10.6;

public double specificHeat = 0.523;

public double heatFusion = 18.8;

public double heatEvap = 422.6;

public double thermalConduct = 21.9;

public double paulingElectro = 1.54;

public double firstIonEnerg = 657.8;

public String oxidationState = "4 3";

public String electronConfig = "[Ar]3d²4s²";

public String lattice = "HEX";

public double latticeConst = 2.95;
}
