package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Rubidium { 



public String name = "Rubidium";

public String symbol = "Rb";

public int atomicNumber = 37;

public double atomicWeight = 85.4678;

public double density = 1.532;

public double meltingPoint = 312.2;

public double boilingPoint = 961;

public double atomicRadius = 248;

public double covalentRadius = 216;

public double ionicRadius = 0;

public double specificVolume = 55.9;

public double specificHeat = 0.36;

public double heatFusion = 2.2;

public double heatEvap = 75.8;

public double thermalConduct = 58.2;

public double paulingElectro = 0.82;

public double firstIonEnerg = 402.8;

public String oxidationState = "1";

public String electronConfig = "[Kr]5s¹";

public String lattice = "BCC";

public double latticeConst = 5.59;
}
