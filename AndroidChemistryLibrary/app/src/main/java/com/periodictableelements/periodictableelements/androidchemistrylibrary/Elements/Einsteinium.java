package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Einsteinium { 



public String name = "Einsteinium";

public String symbol = "Es";

public int atomicNumber = 99;

public double atomicWeight = 252.083;

public double density = 0;

public double meltingPoint = 0;

public double boilingPoint = 1130;

public double atomicRadius = 292;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 1.3;

public double firstIonEnerg = 620;

public String oxidationState = "3";

public String electronConfig = "[Rn]5f¹¹6d7s²";

public String lattice = "-";

public double latticeConst = 0;
}
