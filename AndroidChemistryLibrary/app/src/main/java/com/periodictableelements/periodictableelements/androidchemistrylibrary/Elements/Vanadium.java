package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Vanadium { 



public String name = "Vanadium";

public String symbol = "V";

public int atomicNumber = 23;

public double atomicWeight = 50.9415;

public double density = 6.11;

public double meltingPoint = 2160;

public double boilingPoint = 3650;

public double atomicRadius = 134;

public double covalentRadius = 122;

public double ionicRadius = 0;

public double specificVolume = 8.35;

public double specificHeat = 0.485;

public double heatFusion = 17.5;

public double heatEvap = 460;

public double thermalConduct = 30.7;

public double paulingElectro = 1.63;

public double firstIonEnerg = 650.1;

public String oxidationState = "5 4 3 2 0";

public String electronConfig = "[Ar]3d³4s²";

public String lattice = "BCC";

public double latticeConst = 3.02;
}
