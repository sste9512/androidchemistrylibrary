package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Yttrium { 



public String name = "Yttrium";

public String symbol = "Y";

public int atomicNumber = 39;

public double atomicWeight = 88.90585;

public double density = 4.47;

public double meltingPoint = 1795;

public double boilingPoint = 3611;

public double atomicRadius = 178;

public double covalentRadius = 162;

public double ionicRadius = 0;

public double specificVolume = 19.8;

public double specificHeat = 0.284;

public double heatFusion = 11.5;

public double heatEvap = 367;

public double thermalConduct = 17.2;

public double paulingElectro = 1.22;

public double firstIonEnerg = 615.4;

public String oxidationState = "3";

public String electronConfig = "[Kr]4d¹5s²";

public String lattice = "HEX";

public double latticeConst = 3.65;
}
