package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Sulfur { 



public String name = "Sulfur";

public String symbol = "S";

public int atomicNumber = 16;

public double atomicWeight = 32.066;

public double density = 2.07;

public double meltingPoint = 386;

public double boilingPoint = 717.824;

public double atomicRadius = 127;

public double covalentRadius = 102;

public double ionicRadius = 0;

public double specificVolume = 15.5;

public double specificHeat = 0.732;

public double heatFusion = 1.23;

public double heatEvap = 10.5;

public double thermalConduct = 0.27;

public double paulingElectro = 2.58;

public double firstIonEnerg = 999;

public String oxidationState = "6 4 2 -2";

public String electronConfig = "[Ne]3s²3p";

public String lattice = "ORC";

public double latticeConst = 10.47;
}
